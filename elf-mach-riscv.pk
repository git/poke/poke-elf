/* elf-mach-riscv.pk - ELF RISCV specific definitions.  */

/* Copyright (C) 2024 Jose E. Maorchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* RISCV file flags.  */

var ELF_EF_RISCV_RVC = 0x0001U,
    ELF_EF_RISCV_FLOAT_ABI = 0x0006U,
    ELF_EF_RISCV_FLOAT_ABI_SOFT = 0x0000U,
    ELF_EF_RISCV_FLOAT_ABI_SINGLE = 0x0002U,
    ELF_EF_RISCV_FLOAT_ABI_DOUBLE = 0x0004U,
    ELF_EF_RISCV_FLOAT_ABI_QUAD = 0x0006U,
    ELF_EF_RISCV_RVE = 0x0008U;

elf_config.add_mask
  :machine ELF_EM_RISCV
  :class "file-flags"
  :entries [Elf_Config_Mask { value = ELF_EF_RISCV_RVC,
                              name = "rvc",
                              doc = "File may contain compressed instructions." },
            Elf_Config_Mask { value = ELF_EF_RISCV_FLOAT_ABI_SOFT,
                              name = "float-abi-soft",
                              doc = "File uses the soft-float ABI." },
            Elf_Config_Mask { value = ELF_EF_RISCV_FLOAT_ABI_SINGLE,
                              name = "float-abi-single",
                              doc = "File uses the single-float ABI." },
            Elf_Config_Mask { value = ELF_EF_RISCV_FLOAT_ABI_DOUBLE,
                              name = "float-abi-double",
                              doc = "File uses the double-float ABI." },
            Elf_Config_Mask { value = ELF_EF_RISCV_FLOAT_ABI_QUAD,
                              name = "float-abi-quad",
                              doc = "File uses the quad-float ABI." },
            Elf_Config_Mask { value = ELF_EF_RISCV_RVE,
                              name = "rve",
                              doc = "File uses the 32E base integer instruction." }];

/* RISCV dynamic tags.  */

var ELF_DT_RISCV_VARIANT_CC = ELF_DT_LOPROC + 1;

elf_config.add_enum
  :machine ELF_EM_RISCV
  :class "dynamic-tag-types"
  :entries [Elf_Config_UInt { value = ELF_DT_RISCV_VARIANT_CC,
                              name = "variant-cc" }];

/* RISCV section types.  */

var ELF_SHT_RISCV_ATTRIBUTES = 0x7000_0003U;

elf_config.add_enum
  :machine ELF_EM_RISCV
  :class "section-types"
  :entries [Elf_Config_UInt { value = ELF_SHT_RISCV_ATTRIBUTES,
                              name = "attributes",
                              doc = "Section holds attributes" }];

/* RISCV segment types.  */

var ELF_PT_RISCV_ATTRIBUTES = 0x7000_0003U;

elf_config.add_enum
  :machine ELF_EM_RISCV
  :class "segment-types"
  :entries [Elf_Config_UInt { value = ELF_PT_RISCV_ATTRIBUTES,
                              name = "attributes",
                              doc = "Segment holds attributes." }];

/* RISCV st_other values.  */

var ELF_STO_RISCV_VARIANT_CC = 0x80;

elf_config.add_enum
  :machine ELF_EM_RISCV
  :class "section-other"
  :entries [Elf_Config_UInt { value = ELF_STO_RISCV_VARIANT_CC,
                              name = "variant-cc" }];
