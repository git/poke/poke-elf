/* elf.pk - ELF implementation for GNU poke.  */

/* Copyright (C) 2024 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file contains a Poke description of the ELF (Executable and
 * Linking Format) object file format.  The ELF format is described in
 * the chapter 4 of the SysV ABI.
 *
 * Both ELF32 and ELF64 are supported.
 *
 * In general, we use the same field names used in the C structs
 * documented in the gABI, that are also used in the widely used ELF
 * implementations like in the GNU binutils and also in elfutils.
 * This makes life easier for system hackers who are already familiar
 * with these names.
 */

load "elf-build.pk";
load "elf-config.pk";
load "elf-common.pk";
load "elf-os-gnu.pk";
load "elf-os-linux.pk";
load "elf-os-llvm.pk";
load "elf-os-openbsd.pk";
load "elf-mach-x86-64.pk";
load "elf-mach-aarch64.pk";
load "elf-mach-mips.pk";
load "elf-mach-sparc.pk";
load "elf-mach-arm.pk";
load "elf-mach-bpf.pk";
load "elf-mach-riscv.pk";
load "elf-32.pk";
load "elf-64.pk";
