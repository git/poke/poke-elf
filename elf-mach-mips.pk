/* elf-mach-mips.pk - ELF MIPS specific definitions.  */

/* Copyright (C) 2024 Jose E. Marchesi.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* MIPS relocation types.  */

var ELF_R_MIPS_NONE = 0,
    ELF_R_MIPS_16 = 1,
    ELF_R_MIPS_32 = 2,
    ELF_R_MIPS_REL32 = 3,
    ELF_R_MIPS_26 = 4,
    ELF_R_MIPS_HI16 = 5,
    ELF_R_MIPS_LO16 = 6,
    ELF_R_MIPS_GPREL16 = 7,
    ELF_R_MIPS_LITERAL = 8,
    ELF_R_MIPS_GOT16 = 9,
    ELF_R_MIPS_PC16 = 10,
    ELF_R_MIPS_CALL16 = 11,
    ELF_R_MIPS_GPREL32 = 12,
    ELF_R_MIPS_UNUSED1 = 13,
    ELF_R_MIPS_UNUSED2 = 14,
    ELF_R_MIPS_UNUSED3 = 15,
    ELF_R_MIPS_SHIFT5 = 16,
    ELF_R_MIPS_SHIFT6 = 17,
    ELF_R_MIPS_64 = 18,
    ELF_R_MIPS_GOT_DISP = 19,
    ELF_R_MIPS_GOT_PAGE = 20,
    ELF_R_MIPS_GOT_OFST = 21,
    ELF_R_MIPS_GOT_HI16 = 22,
    ELF_R_MIPS_GOT_LO16 = 23,
    ELF_R_MIPS_SUB = 24,
    ELF_R_MIPS_INSERT_A = 25,
    ELF_R_MIPS_INSERT_B = 26,
    ELF_R_MIPS_DELETE = 27,
    ELF_R_MIPS_HIGHER = 28,
    ELF_R_MIPS_HIGHEST = 29,
    ELF_R_MIPS_CALL_HI16 = 30,
    ELF_R_MIPS_CALL_LO16 = 31,
    ELF_R_MIPS_SCN_DISP = 32,
    ELF_R_MIPS_REL16 = 33,
    ELF_R_MIPS_ADD_IMMEDIATE = 34,
    ELF_R_MIPS_PJUMP = 35,
    ELF_R_MIPS_RELGOT = 36,
    ELF_R_MIPS_JALR = 37,
    ELF_R_MIPS_TLS_DTPMOD32 = 38,
    ELF_R_MIPS_TLS_DTPREL32 = 39,
    ELF_R_MIPS_TLS_DTPMOD64 = 40,
    ELF_R_MIPS_TLS_DTPREL64 = 41,
    ELF_R_MIPS_TLS_GD = 42,
    ELF_R_MIPS_TLS_LDM = 43,
    ELF_R_MIPS_TLS_DTPREL_HI16 = 44,
    ELF_R_MIPS_TLS_DTPREL_LO16 = 45,
    ELF_R_MIPS_TLS_GOTTPREL = 46,
    ELF_R_MIPS_TLS_TPREL32 = 47,
    ELF_R_MIPS_TLS_TPREL64 = 48,
    ELF_R_MIPS_TLS_TPREL_HI16 = 49,
    ELF_R_MIPS_TLS_TPREL_LO16 = 50,
    ELF_R_MIPS_GLOB_DAT = 51,
    ELF_R_MIPS_PC21_S2 = 60,
    ELF_R_MIPS_PC26_S2 = 61,
    ELF_R_MIPS_PC18_S3 = 62,
    ELF_R_MIPS_PC19_S2 = 63,
    ELF_R_MIPS_PCHI16 = 64,
    ELF_R_MIPS_PCLO16 = 65,
    ELF_R_MIPS16_26 = 100,
    ELF_R_MIPS16_GPREL = 101,
    ELF_R_MIPS16_GOT16 = 102,
    ELF_R_MIPS16_CALL16 = 103,
    ELF_R_MIPS16_HI16 = 104,
    ELF_R_MIPS16_LO16 = 105,
    ELF_R_MIPS16_TLS_GD = 106,
    ELF_R_MIPS16_TLS_LDM = 107,
    ELF_R_MIPS16_TLS_DTPREL_HI16 = 108,
    ELF_R_MIPS16_TLS_DTPREL_LO16 = 109,
    ELF_R_MIPS16_TLS_GOTTPREL = 110,
    ELF_R_MIPS16_TLS_TPREL_HI16 = 111,
    ELF_R_MIPS16_TLS_TPREL_LO16 = 112,
    ELF_R_MIPS16_PC16_S1 = 113,
    ELF_R_MIPS_COPY = 126,
    ELF_R_MIPS_JUMP_SLOT = 127,
    ELF_R_MICROMIPS_26_S1 = 133,
    ELF_R_MICROMIPS_HI16 = 134,
    ELF_R_MICROMIPS_LO16 = 135,
    ELF_R_MICROMIPS_GPREL16 = 136,
    ELF_R_MICROMIPS_LITERAL = 137,
    ELF_R_MICROMIPS_GOT16 = 138,
    ELF_R_MICROMIPS_PC7_S1 = 139,
    ELF_R_MICROMIPS_PC10_S1 = 140,
    ELF_R_MICROMIPS_PC16_S1 = 141,
    ELF_R_MICROMIPS_CALL16 = 142,
    ELF_R_MICROMIPS_GOT_DISP = 145,
    ELF_R_MICROMIPS_GOT_PAGE = 146,
    ELF_R_MICROMIPS_GOT_OFST = 147,
    ELF_R_MICROMIPS_GOT_HI16 = 148,
    ELF_R_MICROMIPS_GOT_LO16 = 149,
    ELF_R_MICROMIPS_SUB = 150,
    ELF_R_MICROMIPS_HIGHER = 151,
    ELF_R_MICROMIPS_HIGHEST = 152,
    ELF_R_MICROMIPS_CALL_HI16 = 153,
    ELF_R_MICROMIPS_CALL_LO16 = 154,
    ELF_R_MICROMIPS_SCN_DISP = 155,
    ELF_R_MICROMIPS_JALR = 156,
    ELF_R_MICROMIPS_HI0_LO16 = 157,
    ELF_R_MICROMIPS_TLS_GD = 162,
    ELF_R_MICROMIPS_TLS_LDM = 163,
    ELF_R_MICROMIPS_TLS_DTPREL_HI16 = 164,
    ELF_R_MICROMIPS_TLS_DTPREL_LO16 = 165,
    ELF_R_MICROMIPS_TLS_GOTTPREL = 166,
    ELF_R_MICROMIPS_TLS_TPREL_HI16 = 169,
    ELF_R_MICROMIPS_TLS_TPREL_LO16 = 170,
    ELF_R_MICROMIPS_GPREL7_S2 = 172,
    ELF_R_MICROMIPS_PC23_S2 = 173,
    ELF_R_MIPS_PC32 = 248,
    ELF_R_MIPS_EH = 249,
    ELF_R_MIPS_GNU_REL16_S2 = 250,
    ELF_R_MIPS_GNU_VTINHERIT = 253,
    ELF_R_MIPS_GNU_VTENTRY = 254;

elf_config.add_enum
  :machine ELF_EM_MIPS
  :class "reloc-types"
  :entries [Elf_Config_UInt { value = ELF_R_MIPS_NONE, name = "NONE" },
            Elf_Config_UInt { value = ELF_R_MIPS_16, name = "16" },
            Elf_Config_UInt { value = ELF_R_MIPS_32, name = "32" },
            Elf_Config_UInt { value = ELF_R_MIPS_REL32, name = "REL32" },
            Elf_Config_UInt { value = ELF_R_MIPS_26, name = "26" },
            Elf_Config_UInt { value = ELF_R_MIPS_HI16, name = "HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS_LO16, name = "LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS_GPREL16, name = "GPREL16" },
            Elf_Config_UInt { value = ELF_R_MIPS_LITERAL, name = "LITERAL" },
            Elf_Config_UInt { value = ELF_R_MIPS_GOT16, name = "GOT16" },
            Elf_Config_UInt { value = ELF_R_MIPS_PC16, name = "PC16" },
            Elf_Config_UInt { value = ELF_R_MIPS_CALL16, name = "CALL16" },
            Elf_Config_UInt { value = ELF_R_MIPS_GPREL32, name = "GPREL32" },
            Elf_Config_UInt { value = ELF_R_MIPS_UNUSED1, name = "UNUSED1" },
            Elf_Config_UInt { value = ELF_R_MIPS_UNUSED2, name = "UNUSED2" },
            Elf_Config_UInt { value = ELF_R_MIPS_UNUSED3, name = "UNUSED3" },
            Elf_Config_UInt { value = ELF_R_MIPS_SHIFT5, name = "SHIFT5" },
            Elf_Config_UInt { value = ELF_R_MIPS_SHIFT6, name = "SHIFT6" },
            Elf_Config_UInt { value = ELF_R_MIPS_64, name = "64" },
            Elf_Config_UInt { value = ELF_R_MIPS_GOT_DISP, name = "GOT_DISP" },
            Elf_Config_UInt { value = ELF_R_MIPS_GOT_PAGE, name = "GOT_PAGE" },
            Elf_Config_UInt { value = ELF_R_MIPS_GOT_OFST, name = "GOT_OFST" },
            Elf_Config_UInt { value = ELF_R_MIPS_GOT_HI16, name = "GOT_HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS_GOT_LO16, name = "GOT_LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS_SUB, name = "SUB" },
            Elf_Config_UInt { value = ELF_R_MIPS_INSERT_A, name = "INSERT_A" },
            Elf_Config_UInt { value = ELF_R_MIPS_INSERT_B, name = "INSERT_B" },
            Elf_Config_UInt { value = ELF_R_MIPS_DELETE, name = "DELETE" },
            Elf_Config_UInt { value = ELF_R_MIPS_HIGHER, name = "HIGHER" },
            Elf_Config_UInt { value = ELF_R_MIPS_HIGHEST, name = "HIGHEST" },
            Elf_Config_UInt { value = ELF_R_MIPS_CALL_HI16, name = "CALL_HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS_CALL_LO16, name = "CALL_LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS_SCN_DISP, name = "SCN_DISP" },
            Elf_Config_UInt { value = ELF_R_MIPS_REL16, name = "REL16" },
            Elf_Config_UInt { value = ELF_R_MIPS_ADD_IMMEDIATE, name = "ADD_IMMEDIATE" },
            Elf_Config_UInt { value = ELF_R_MIPS_PJUMP, name = "PJUMP" },
            Elf_Config_UInt { value = ELF_R_MIPS_RELGOT, name = "RELGOT" },
            Elf_Config_UInt { value = ELF_R_MIPS_JALR, name = "JALR" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_DTPMOD32, name = "TLS_DTPMOD32" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_DTPREL32, name = "TLS_DTPREL32" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_DTPMOD64, name = "TLS_DTPMOD64" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_DTPREL64, name = "TLS_DTPREL64" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_GD, name = "TLS_GD" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_LDM, name = "TLS_LDM" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_DTPREL_HI16, name = "TLS_DTPREL_HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_DTPREL_LO16, name = "TLS_DTPREL_LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_GOTTPREL, name = "TLS_GOTTPREL" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_TPREL32, name = "TLS_TPREL32" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_TPREL64, name = "TLS_TPREL64" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_TPREL_HI16, name = "TLS_TPREL_HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS_TLS_TPREL_LO16, name = "TLS_TPREL_LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS_GLOB_DAT, name = "GLOB_DAT" },
            Elf_Config_UInt { value = ELF_R_MIPS_PC21_S2, name = "PC21_S2" },
            Elf_Config_UInt { value = ELF_R_MIPS_PC26_S2, name = "PC26_S2" },
            Elf_Config_UInt { value = ELF_R_MIPS_PC18_S3, name = "PC18_S3" },
            Elf_Config_UInt { value = ELF_R_MIPS_PC19_S2, name = "PC19_S2" },
            Elf_Config_UInt { value = ELF_R_MIPS_PCHI16, name = "PCHI16" },
            Elf_Config_UInt { value = ELF_R_MIPS_PCLO16, name = "PCLO16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_GPREL, name = "MIPS16_GPREL" },
            Elf_Config_UInt { value = ELF_R_MIPS16_GOT16, name = "MIPS16_GOT16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_CALL16, name = "MIPS16_CALL16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_HI16, name = "MIPS16_HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_LO16, name = "MIPS16_LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_TLS_GD, name = "MIPS16_TLS_GD" },
            Elf_Config_UInt { value = ELF_R_MIPS16_TLS_LDM, name = "MIPS16_TLS_LDM" },
            Elf_Config_UInt { value = ELF_R_MIPS16_TLS_DTPREL_HI16, name = "MIPS16_TLS_DTPREL_HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_TLS_DTPREL_LO16, name = "MIPS16_TLS_DTPREL_LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_TLS_GOTTPREL, name = "MIPS16_TLS_GOTTPREL" },
            Elf_Config_UInt { value = ELF_R_MIPS16_TLS_TPREL_HI16, name = "MIPS16_TLS_TPREL_HI16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_TLS_TPREL_LO16, name = "MIPS16_TLS_TPREL_LO16" },
            Elf_Config_UInt { value = ELF_R_MIPS16_PC16_S1, name = "MIPS16_PC16_S1" },
            Elf_Config_UInt { value = ELF_R_MIPS_COPY, name = "COPY" },
            Elf_Config_UInt { value = ELF_R_MIPS_JUMP_SLOT, name = "JUMP_SLOT" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_26_S1, name = "MICROMIPS_26_S1" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_HI16, name = "MICROMIPS_HI16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_LO16, name = "MICROMIPS_LO16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GPREL16, name = "MICROMIPS_GPREL16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_LITERAL, name = "MICROMIPS_LITERAL" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GOT16, name = "MICROMIPS_GOT16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_PC7_S1, name = "MICROMIPS_PC7_S1" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_PC10_S1, name = "MICROMIPS_PC10_S1" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_PC16_S1, name = "MICROMIPS_PC16_S1" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_CALL16, name = "MICROMIPS_CALL16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GOT_DISP, name = "MICROMIPS_GOT_DISP" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GOT_PAGE, name = "MICROMIPS_GOT_PAGE" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GOT_OFST, name = "MICROMIPS_GOT_OFST" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GOT_HI16, name = "MICROMIPS_GOT_HI16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GOT_LO16, name = "MICROMIPS_GOT_LO16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_SUB, name = "MICROMIPS_SUB" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_HIGHER, name = "MICROMIPS_HIGHER" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_HIGHEST, name = "MICROMIPS_HIGHEST" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_CALL_HI16, name = "MICROMIPS_CALL_HI16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_CALL_LO16, name = "MICROMIPS_CALL_LO16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_SCN_DISP, name = "MICROMIPS_SCN_DISP" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_JALR, name = "MICROMIPS_JALR" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_HI0_LO16, name = "MICROMIPS_HI0_LO16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_TLS_GD, name = "MICROMIPS_TLS_GD" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_TLS_LDM, name = "MICROMIPS_TLS_LDM" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_TLS_DTPREL_HI16, name = "MICROMIPS_TLS_DTPREL_HI16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_TLS_DTPREL_LO16, name = "MICROMIPS_TLS_DTPREL_LO16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_TLS_GOTTPREL, name = "MICROMIPS_TLS_GOTTPREL" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_TLS_TPREL_HI16, name = "MICROMIPS_TLS_TPREL_HI16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_TLS_TPREL_LO16, name = "MICROMIPS_TLS_TPREL_LO16" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_GPREL7_S2, name = "MICROMIPS_GPREL7_S2" },
            Elf_Config_UInt { value = ELF_R_MICROMIPS_PC23_S2, name = "MICROMIPS_PC23_S2" },
            Elf_Config_UInt { value = ELF_R_MIPS_PC32, name = "PC32" },
            Elf_Config_UInt { value = ELF_R_MIPS_EH, name = "EH" },
            Elf_Config_UInt { value = ELF_R_MIPS_GNU_REL16_S2, name = "GNU_REL16_S2" },
            Elf_Config_UInt { value = ELF_R_MIPS_GNU_VTINHERIT, name = "GNU_VTINHERIT" },
            Elf_Config_UInt { value = ELF_R_MIPS_GNU_VTENTRY, name = "GNU_VTENTRY" }];

/* MIPS file flags.  */

var ELF_EF_MIPS_NOREORDER     = 0x0000_0001U,
    ELF_EF_MIPS_PIC           = 0x0000_0002U,
    ELF_EF_MIPS_CPIC          = 0x0000_0004U,
    ELF_EF_MIPS_XGOT          = 0x0000_0008U,
    ELF_EF_MIPS_UCODE         = 0x0000_0010U,
    ELF_EF_MIPS_ABI2          = 0x0000_0020U,
    ELF_EF_MIPS_OPTIONS_FIRST = 0x0000_0080U,
    ELF_EF_MIPS_32BITMODE     = 0x0000_0010U,
    ELF_EF_MIPS_FP64          = 0x0000_0200U,
    EFL_EF_MIPS_NAN2008       = 0x0000_0400U,
    ELF_EF_MIPS_ARCH_ASE      = 0x0f00_0000U,
    ELF_EF_MIPS_ARCH_ASE_MDMX = 0x0800_0000U,
    ELF_EF_MIPS_ARCH_ASE_M16  = 0x0400_0000U,
    ELF_EF_MIPS_ARCH_ASE_MICROMIPS = 0x0200_0000U,
    ELF_EF_MIPS_ARCH          = 0xf000_0000U,
    ELF_EF_MIPS_ABI           = 0x0000_f000U,
    ELF_EF_MIPS_MACH          = 0x00ff_0000U;

elf_config.add_mask
  :machine ELF_EM_MIPS
  :class "file-flags"
  :entries [Elf_Config_Mask { value = ELF_EF_MIPS_NOREORDER, name = "noreorder",
                              doc = "At least one .noreorder directive appears in the source." }];

/* MIPS architectures encoded in the ELF_EF_MIPS_ARCH bits of
   the file flags.  */

var ELF_E_MIPS_ARCH_1  = 0x0000_0000U,
    ELF_E_MIPS_ARCH_2  = 0x1000_0000U,
    ELF_E_MIPS_ARCH_3  = 0x2000_0000U,
    ELF_E_MIPS_ARCH_4  = 0x3000_0000U,
    ELF_E_MIPS_ARCH_5  = 0x4000_0000U,
    ELF_E_MIPS_ARCH_32 = 0x5000_0000U,
    ELF_E_MIPS_ARCH_64 = 0x6000_0000U,
    ELF_E_MIPS_ARCH_32R2 = 0x7000_0000U,
    ELF_E_MIPS_ARCH_64R2 = 0x8000_0000U,
    ELF_E_MIPS_ARCH_32R6 = 0x9000_0000U,
    ELF_E_MIPS_ARCH_64R6 = 0xa000_0000U;

elf_config.add_enum
  :machine ELF_EM_MIPS
  :class "mips-architectures"
  :entries [Elf_Config_UInt { value = ELF_E_MIPS_ARCH_1, name = "mips1",
                              doc = "-mips1 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_2, name = "mips2",
                              doc = "-mips2 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_3, name = "mips3",
                              doc = "-mips3 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_4, name = "mips4",
                              doc = "-mips4 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_5, name = "mips5",
                              doc = "-mips5 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_32, name = "mips32",
                              doc = "-mips32 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_64, name = "mips64",
                              doc = "-mips64 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_32R2, name = "mips32r2",
                              doc = "-mips32r2 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_64R2, name = "mips64r2",
                              doc = "-mips64r2 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_32R6, name = "mips32r6",
                              doc = "-mips32r6 code." },
            Elf_Config_UInt { value = ELF_E_MIPS_ARCH_64R6, name = "mips64r6",
                              doc = "-mips64r6 code." }];

/* MIPS machines encoded in the ELF_EF_MIPS_MACH bits of
   the file flags.  */

var ELF_E_MIPS_ABI_O32    = 0x0000_1000U,
    ELF_E_MIPS_ABI_O64    = 0x0000_2000U,
    ELF_E_MIPS_ABI_EABI32 = 0x0000_3000U,
    ELF_E_MIPS_ABI_EABI64 = 0x0000_4000U;

elf_config.add_enum
  :machine ELF_EM_MIPS
  :class "mips-abis"
  :entries [Elf_Config_UInt { value = ELF_E_MIPS_ABI_O32, name = "o32",
                              doc = "The original o32 abi." },
            Elf_Config_UInt { value = ELF_E_MIPS_ABI_O64, name = "o64",
                              doc = "o32 extended to work on 64 bit architectures." },
            Elf_Config_UInt { value = ELF_E_MIPS_ABI_EABI32, name = "eabi32",
                              doc = "EABI in 32 bit mode." },
            Elf_Config_UInt { value = ELF_E_MIPS_ABI_EABI64, name = "eabi64",
                              doc = "EABI in 64 bit mode." }];

var ELF_E_MIPS_MACH_3900 = 0x0081_0000U,
    ELF_E_MIPS_MACH_4010 = 0x0082_0000U,
    ELF_E_MIPS_MACH_4100 = 0x0083_0000U,
    ELF_E_MIPS_MACH_4650 = 0x0085_0000U,
    ELF_E_MIPS_MACH_4120 = 0x0087_0000U,
    ELF_E_MIPS_MACH_4111 = 0x0088_0000U,
    ELF_E_MIPS_MACH_SB1  = 0x008a_0000U,
    ELF_E_MIPS_MACH_OCTEON  = 0x008b_0000U,
    ELF_E_MIPS_MACH_XLR     = 0x008c_0000U,
    ELF_E_MIPS_MACH_OCTEON2 = 0x008d_0000U,
    ELF_E_MIPS_MACH_OCTEON3 = 0x008e_0000U,
    ELF_E_MIPS_MACH_5400   = 0x0091_0000U,
    ELF_E_MIPS_MACH_5900   = 0x0092_0000U,
    ELF_E_MIPS_MACH_IAMR2  = 0x0093_0000U,
    ELF_E_MIPS_MACH_5500   = 0x0098_0000U,
    ELF_E_MIPS_MACH_9000   = 0x0099_0000U,
    ELF_E_MIPS_MACH_LS2E   = 0x00A0_0000U,
    ELF_E_MIPS_MACH_LS2F   = 0x00A1_0000U,
    ELF_E_MIPS_MACH_GS464  = 0x00A2_0000U,
    ELF_E_MIPS_MACH_GS464E = 0x00A3_0000U,
    ELF_E_MIPS_MACH_GS264E = 0x00A4_0000U;

elf_config.add_enum
  :machine ELF_EM_MIPS
  :class "mips-machines"
  :entries [Elf_Config_UInt { value = ELF_E_MIPS_MACH_3900, name = "3900" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_4010, name = "4010" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_4100, name = "4100" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_4650, name = "4650" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_4120, name = "4120" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_4111, name = "4111" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_SB1 , name = "SB1 " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_OCTEON , name = "OCTEON " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_XLR    , name = "XLR    " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_OCTEON2, name = "OCTEON2" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_OCTEON3, name = "OCTEON3" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_5400  , name = "5400  " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_5900  , name = "5900  " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_IAMR2 , name = "IAMR2 " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_5500  , name = "5500  " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_9000  , name = "9000  " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_LS2E  , name = "LS2E  " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_LS2F  , name = "LS2F  " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_GS464 , name = "GS464 " },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_GS464E, name = "GS464E" },
            Elf_Config_UInt { value = ELF_E_MIPS_MACH_GS264E, name = "GS264E" }];

/* MIPS section indices.  */

var ELF_SHN_MIPS_ACOMMON = ELF_SHN_LORESERVE;

elf_config.add_enum
  :machine ELF_EM_MIPS
  :class "section-indices"
  :entries [Elf_Config_UInt { value = ELF_SHN_MIPS_ACOMMON, name = "acommon",
                              doc = "Defined and allocated common symbol." }];

/* MIPS section types.  */

var ELF_SHT_MIPS_MSYM = 0x7000_0000U,
    ELF_SHT_MIPS_MSYM = 0x7000_0001U,
    ELF_SHT_MIPS_CONFLICT = 0x7000_0002U,
    ELF_SHT_MIPS_GPTAB = 0x7000_0003U,
    ELF_SHT_MIPS_UCODE = 0x7000_0004U,
    ELF_SHT_MIPS_DEBUG = 0x7000_0005U,
    ELF_SHT_MIPS_REGINFO = 0x7000_0006U,
    ELF_SHT_MIPS_PACKAGE = 0x7000_0007U,
    ELF_SHT_MIPS_PACKSYM = 0x7000_0008U,
    ELF_SHT_MIPS_RELD = 0x7000_0009U,
    ELF_SHT_MIPS_IFACE = 0x7000_000bU,
    ELF_SHT_MIPS_CONTENT = 0x7000_000cU,
    ELF_SHT_MIPS_OPTIONS = 0x7000_000dU,
    ELF_SHT_MIPS_SHDR = 0x7000_0010U,
    ELF_SHT_MIPS_FDESC = 0x7000_0011U,
    ELF_SHT_MIPS_EXTSYM = 0x7000_0012U,
    ELF_SHT_MIPS_DENSE = 0x7000_0013U,
    ELF_SHT_MIPS_PDESC = 0x7000_0014U,
    ELF_SHT_MIPS_LOCSYM = 0x7000_0015U,
    ELF_SHT_MIPS_AUXSYM = 0x7000_0016U,
    ELF_SHT_MIPS_OPTSYM = 0x7000_0017U,
    ELF_SHT_MIPS_LOCSTR = 0x7000_0018U,
    ELF_SHT_MIPS_LINE = 0x7000_0019U,
    ELF_SHT_MIPS_RFDES = 0x7000_001aU,
    ELF_SHT_MIPS_DELTASYM = 0x7000_001bU,
    ELF_SHT_MIPS_DELTAINST = 0x7000_001cU,
    ELF_SHT_MIPS_DELTACLASS = 0x7000_001dU,
    ELF_SHT_MIPS_DWARF = 0x7000_001eU,
    ELF_SHT_MIPS_DELTADECL = 0x7000_001fU,
    ELF_SHT_MIPS_SYMBOL_LIB = 0x7000_0020U,
    ELF_SHT_MIPS_EVENTS = 0x7000_0021U,
    ELF_SHT_MIPS_TRANSLATE = 0x7000_0022U,
    ELF_SHT_MIPS_PIXIE = 0x7000_0023U,
    ELF_SHT_MIPS_XLATE = 0x7000_0024U,
    ELF_SHT_MIPS_XLATE_DEBUG = 0x7000_0025U,
    ELF_SHT_MIPS_WHIRL = 0x7000_0026U,
    ELF_SHT_MIPS_EH_REGION = 0x7000_0027U,
    ELF_SHT_MIPS_XLATE_OLD = 0x7000_0028U,
    ELF_SHT_MIPS_PDR_EXCEPTION = 0x7000_0029U,
    ELF_SHT_MIPS_ABIFLAGS = 0x7000_002aU,
    ELF_SHT_MIPS_XHASH = 0x7000_002bU;

elf_config.add_enum
  :machine ELF_EM_MIPS
  :class "section-types"
  :entries [Elf_Config_UInt { value = ELF_SHT_MIPS_MSYM, name = "MSYM",
                              doc = "Section contains the set of dynamic shared"
                                    + " objects used when statically linking." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_MSYM, name = "MSYM",
                              doc = "Used on Irix 5." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_CONFLICT, name = "CONFLICT",
                              doc = "Section contains list of symbols whose definitions"
                                    + " conflict with symbols defined in shared objects."},
            Elf_Config_UInt { value = ELF_SHT_MIPS_GPTAB, name = "GPTAB",
                              doc = "Section contains the global pointer table." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_UCODE, name = "UCODE",
                              doc = "Section contains microcode information.  The exact"
                                    + "format is unspecified." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_DEBUG, name = "DEBUG",
                              doc = "Section contains some sort of debugging information."
                                    + "Probably ECOFF symbols." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_REGINFO, name = "REGINFO",
                              doc = "Section contains register usage information." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_PACKAGE, name = "PACKAGE" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_PACKSYM, name = "PACKSYM" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_RELD, name = "RELD" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_IFACE, name = "IFACE",
                              doc = "Section contains interface information." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_CONTENT, name = "CONTENT",
                              doc = "Section contains description of contents of another section." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_OPTIONS, name = "OPTIONS",
                              doc = "Section contains miscellaneous options." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_SHDR, name = "SHDR" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_FDESC, name = "FDESC" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_EXTSYM, name = "EXTSYM" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_DENSE, name = "DENSE" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_PDESC, name = "PDESC" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_LOCSYM, name = "LOCSYM" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_AUXSYM, name = "AUXSYM" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_OPTSYM, name = "OPTSYM" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_LOCSTR, name = "LOCSTR" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_LINE, name = "LINE" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_RFDES, name = "RFDES" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_DELTASYM, name = "DELTASYM",
                              doc = "Delta C++: symbol table." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_DELTAINST, name = "DELTAINST",
                              doc = "Delta C++: instance table." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_DELTACLASS, name = "DELTACLASS",
                              doc = "Delta C++: class table." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_DWARF, name = "DWARF",
                              doc = "DWARF debugging section." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_DELTADECL, name = "DELTADECL",
                              doc = "Delta C++: declarations" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_SYMBOL_LIB, name = "SYMBOL_LIB",
                              doc = "List of libraries the binary depends on.  Includes"
                                    + "a time stamp, version number." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_EVENTS, name = "EVENTS",
                              doc = "Events section." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_TRANSLATE, name = "TRANSLATE" },
            Elf_Config_UInt { value = ELF_SHT_MIPS_PIXIE, name = "PIXIE",
                              doc = "Special pixie sections." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_XLATE, name = "XLATE",
                              doc = "Addres translation table (for debug info)." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_XLATE_DEBUG, name = "XLATE_DEBUG",
                              doc = "SGI internal address translation table (for debug info)." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_WHIRL, name = "WHIRL",
                              doc = "Intermediate code." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_EH_REGION, name = "EH_REGION",
                              doc = "C++ exceptino handling region info." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_XLATE_OLD, name = "XLATE_OLD",
                              doc = "Obsolete address translation table (for debug info)." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_PDR_EXCEPTION, name = "PDR_EXCEPTION",
                              doc = "Runtime procedure descriptor table exception information (ucode)." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_ABIFLAGS, name = "ABIFLAGS",
                              doc = "ABI related flags section." },
            Elf_Config_UInt { value = ELF_SHT_MIPS_XHASH, name = "XHASH",
                              doc = "GNU style symbol hash table with xlat." }];

/* MIPS section flags.  */

var ELF_SHF_MIPS_GPRE = 0x1000_0000U,
    ELF_SHF_MIPS_MERGE = 0x2000_0000U,
    ELF_SHF_MIPS_ADDR = 0x4000_0000U,
    ELF_SHF_MIPS_STRING = 0x8000_0000U,
    ELF_SHF_MIPS_NOSTRIP = 0x0800_0000U,
    ELF_SHF_MIPS_LOCAL = 0x0400_0000U,
    ELF_SHF_MIPS_NAMES = 0x0200_0000U,
    ELF_SHF_MIPS_NODUPES = 0x0100_0000U;

elf_config.add_mask
  :machine ELF_EM_MIPS
  :class "section-flags"
  :entries [Elf_Config_Mask { value = ELF_SHF_MIPS_GPRE, name = "gpre",
                              doc = "This section must be in the global data area." },
            Elf_Config_Mask { value = ELF_SHF_MIPS_MERGE, name = "merge",
                              doc = "This section should be merged." },
            Elf_Config_Mask { value = ELF_SHF_MIPS_ADDR, name = "addr",
                              doc = "This section contains address data of size"
                                    + " implied by section element size." },
            Elf_Config_Mask { value = ELF_SHF_MIPS_STRING, name = "string",
                              doc = "This section contains string data." },
            Elf_Config_Mask { value = ELF_SHF_MIPS_NOSTRIP, name = "nostrip",
                              doc = "This section may not be stripped." },
            Elf_Config_Mask { value = ELF_SHF_MIPS_LOCAL, name = "local",
                              doc = "This section is local to threads." },
            Elf_Config_Mask { value = ELF_SHF_MIPS_NAMES, name = "names",
                              doc = "Linker should generate implicit weak names for this section." },
            Elf_Config_Mask { value = ELF_SHF_MIPS_NODUPES, name = "nodupes",
                              doc = "Section contains text/data which may be replicated"
                                    + " in other sections.  Linker should retain only one copy." }];

/* MIPS segment types.  */

var ELF_PT_MIPS_REGINFO = 0x7000_0000U,
    ELF_PT_MIPS_RTPROC = 0x7000_0001U,
    ELF_PT_MIPS_OPTIONS = 0x7000_0002U,
    ELF_PT_MIPS_ABIFLAGS = 0x7000_0003U;

elf_config.add_enum
  :machine ELF_EM_MIPS
  :class "segment-types"
  :entries [Elf_Config_UInt { value = ELF_PT_MIPS_REGINFO, name = "reginfo",
                              doc = "Register usage information.  Identifies one .reginfo section." },
            Elf_Config_UInt { value = ELF_PT_MIPS_RTPROC, name = "rtproc",
                              doc = "Runtime procedure table." },
            Elf_Config_UInt { value = ELF_PT_MIPS_OPTIONS, name = "options",
                              doc = ".MIPS.options section." },
            Elf_Config_UInt { value = ELF_PT_MIPS_ABIFLAGS, name = "abiflags",
                              doc = "Records ABI related flags." }];

/* Values for the l_flags field of an Elf32_MIPS_Lib.  */

var ELF_MIPS_LL_EXACT_MATCH = 0x0000_0001U,
    ELF_MIPS_LL_IGNORE_INT_VER = 0x0000_0002U,
    ELF_MIPS_LL_REQUIRE_MINOR = 0x0000_0004U,
    ELF_MIPS_LL_EXPORTS = 0x0000_0008U,
    ELF_MIPS_LL_DELAY_LOAD = 0x0000_0010U,
    ELF_MIPS_LL_DELTA = 0x0000_0020U;

elf_config.add_mask
  :machine ELF_EM_MIPS
  :class "mips-l-flags"
  :entries [Elf_Config_Mask { value = ELF_MIPS_LL_EXACT_MATCH, name = "exact-match",
                              doc = "Require an exact match at runtime." },
            Elf_Config_Mask { value = ELF_MIPS_LL_IGNORE_INT_VER, name = "ignore-int-ver",
                              doc = "Ignore version incompatibilities at runtime." },
            Elf_Config_Mask { value = ELF_MIPS_LL_REQUIRE_MINOR, name = "require-minor",
                              doc = "Require matching minor version number." },
            Elf_Config_Mask { value = ELF_MIPS_LL_EXPORTS, name = "exports" },
            Elf_Config_Mask { value = ELF_MIPS_LL_DELAY_LOAD, name = "delay-load",
                              doc = "Delay loading of this library until really needed." },
            Elf_Config_Mask { value = ELF_MIPS_LL_DELTA, name = "delta",
                              doc = "Delta C++ stuff???" }];

/* Sections of type ELF_SHT_MIPS_LIBLIST contain an array of the following
   structure.  The sh_link field is the section index of the string table.
   The sh_info field is the number of entries in the section.  */

type Elf32_MIPS_Lib =
  struct
  {
    /* String table index for name of shared object.  */
    uint<32> l_name;
    uint<32> l_time_stamp;
    /* Checksum of symbol names and common sizes.  */
    uint<32> l_checksum;
    /* String table index for version.  */
    uint<32> l_version;
    /* Flags.  */
    uint<32> l_flags : elf_config.check_mask ("mips-l-flags", ELF_EM_MIPS, l_flags);

    method _print_l_flags = void:
    {
      printf "#<%s>", elf_config.format_mask ("mips-l-flags", ELF_EM_MIPS, l_flags);
    }
  };

/* Sections of type ELF_SHT_MIPS_CONFLICT contain arrays of indices into the .dynsym
   section.  Each element of the array has the following type.  */

type Elf32_MIPS_Conflict = uint<4>;
type Elf64_MIPS_Conflict = uint<8>;

/* Sections of type ELF_SHT_MIPS_GPTAB contain a single instance of an
   Elf32_MIPS_Gptab_Entry, followed by zero or more Elf32_Gptab_Entry. */

type Elf32_MIPS_Gptab_Header =
  struct
  {
    /* -G value actually used for this object file.  */
    uint<32> gt_current_g_value;
    uint<32> gt_unused;
  };

type Elf32_MIPS_Gptab_Entry =
  struct
  {
    /* If this -G argument has been used... */
    uint<32> gt_g_value;
    /* ... this many GP section bytes would be required.  */
    uint<32> gt_bytes;
  };

/* Sections of type SHT_MIPS_REGINFO contain a single instance
   of Elf32_MIPS_RegInfo.  */

type Elf32_MIPS_RegInfo =
  struct
  {
    /* Mask of general purpose registers used.  */
    uint<32> ri_gprmask;
    /* Mask of co-processor registers used.  */
    uint<32>[4] ri_cprmask;
    /* GP register value for this object file.  */
    uint<32> ri_gp_value;
  };
