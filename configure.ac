dnl configure.ac for the poke ELF pickle
dnl
dnl Please process this file with autoconf to get a 'configure'
dnl script.

dnl Copyright (C) 2024 Jose E. Marchesi

dnl This program is free software: you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation, either version 3 of the License, or
dnl (at your option) any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program.  If not, see <http://www.gnu.org/licenses/>.

AC_INIT([poke-elf], [1.0], [poke-devel@gnu.org], [poke-elf],
        [https://www.jemarch.net/poke-elf])
AC_CONFIG_AUX_DIR([build-aux])

AM_INIT_AUTOMAKE([foreign])
AC_REQUIRE_AUX_FILE([tap-driver.sh])

dnl Check for an installed poke, and minimum required version to test
dnl this pickle.  If found, it puts the absolute path in the POKE
dnl variable, and AC_SUBST it.  If not found, it sets POKE to :.

run_pickle_tests=no
PK_PROG_POKE([POKE],[4.0])

if test "x$POKE" != "x:"; then
   dnl pktest is also required to run the testsuite.
   have_pktest=no
   PK_CHECK_PICKLE([$POKE],[pktest.pk],[run_pickle_tests=yes])
fi   

AM_CONDITIONAL([RUN_TESTS], [test "x$run_pickle_tests" = "xyes"])

dnl Configure and output files.

AC_CONFIG_FILES([Makefile elf-build.pk])
AC_CONFIG_FILES([test-elf.pk], [chmod +x test-elf.pk])
AC_OUTPUT

dnl Emit some diagnostics at the end.

if test "x$POKE" = "x:"; then
  AC_MSG_WARN([poke >= 4.0 is required to run this pickle's testsuite])
fi
